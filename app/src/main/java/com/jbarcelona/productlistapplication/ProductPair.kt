package com.jbarcelona.productlistapplication

data class ProductPair<String, Double>(var first: String, var second: Double)