package com.jbarcelona.productlistapplication

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.TextView
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView

class MainActivity : AppCompatActivity() {
    private lateinit var productAdapter: ProductAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val rvProductItems = findViewById<RecyclerView>(R.id.rv_products)
        val tvTotal = findViewById<TextView>(R.id.tv_total)

        val productItems = arrayListOf<ProductPair<String, Double>>()
        productItems.add(ProductPair("Toothpaste", 10.00))
        productItems.add(ProductPair("Toothbrush", 8.00))
        productItems.add(ProductPair("Mouthwash", 15.00))
        productItems.add(ProductPair("Hand Soap", 18.00))
        productItems.add(ProductPair("Candies", 16.00))
        productItems.add(ProductPair("Cotton", 10.00))
        productItems.add(ProductPair("Body Wash", 20.00))
        productItems.add(ProductPair("Floss", 3.00))
        productItems.add(ProductPair("Electric Toothbrush", 30.00))
        productItems.add(ProductPair("Cologne", 10.00))

        var totalPrice = 0.0
        val prices = arrayListOf<Double>()
        productItems.forEach {
            prices.add(it.second)
        }
        prices.shuffle()
        prices.forEachIndexed { index, priceItem ->
            totalPrice += priceItem
            productItems[index].second = priceItem
        }

        tvTotal.text = "$ ${String.format("%.2f", totalPrice)}"
        productAdapter = ProductAdapter(productItems)
        rvProductItems.adapter = productAdapter
        rvProductItems.layoutManager = LinearLayoutManager(this)
    }
}